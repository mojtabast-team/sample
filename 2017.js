/**
 *
 * This is one the app pages for Flowtify based on React Native.
 *
 * Used advanced topics in React Native like HOC
 *
 */

import React from 'react';
import {View, Text} from 'react-native'

// You can check it out in helpers/TaskHelpers
import TaskHelpers from './helpers/TaskHelpers'

import Layout from '../main/Layout'

import H from '../components/Typography/H'
import Description from '../components/Task/Description'
import Separator from '../components/Separator'
import Nothing from '../components/Comments/Nothing'
import StatusColor from '../components/Task/StatusColor'
import Attachments from '../components/Task/Attachments'

class Task extends React.Component {


  componentWillMount(){

    this.props.initTask();
  }

  render() {

    const task = this.props.task;
    console.log('Task Data', task);

    if( !!task == false ){

      return <View>
        <Text>Task not found.</Text>
      </View>
    }


    return (
      <View>

        <H>
          <StatusColor
            status={{
              resolved: task.resolved,
              finished: task.finished,
              success: task.success,
              critical: task.critical,
              timedOut: task.timedOut,
            }}
          >
            {task.title}
          </StatusColor>
        </H>

        <Description>
          {task.description}
        </Description>

        <Attachments files={task.attachments ? task.attachments : []} />

        <Separator />

        <Nothing />

      </View>
    );
  }


}

const TaskContainer = TaskHelpers(Task);

export default Layout(TaskContainer);