/**
 *
 * Video module
 *
 * I used this module for creating video player in this page: http://somethingamazing.mojtabast.me/mehrtan/videos.html
 *
 */

;(function(window) {
  'use strict';

  var video = function (el) {

    this.el = el;
    if (el == undefined)
      this.el = this.check();

    if (this.el == false)
      return false;

    // Variables. We need them in module
    this.container = document.querySelector('.video-container');
    this.overlayElement = '';
    this.barLineElement = '';
    this.barCurrentTimeElement = '';

    this.poster = '';
    this.url = '';
    this.video_el = '';
    this.overlayPoster = true;

    // Init module
    this.init();

    // Let's go.
    this.run();
  };

  video.prototype.check = function () {

    if (document.querySelector('.current'))
      return document.querySelector('.current');
    else
      return false;
  };

  video.prototype.init = function () {

    this.poster = this.el.getAttribute('data-poster');
    this.url = this.el.getAttribute('data-video');
  };

  video.prototype.end = function () {

    this.playButton.className = 'icon-replay';
    this.playButton.classList.add('changeButtonIcon');
    this.overlayElement.classList.remove('video-overlay-bg-hide');
  };

  video.prototype.play = function () {

    var that = this;
    var btn = this.playButton;

    if (btn.classList.contains('icon-controller-play') || btn.classList.contains('icon-replay')) {

      if (btn.classList.contains('icon-replay')) {

        btn.className = 'icon-controller-play';
      }

      //Pause
      this.overlayElement.classList.add('video-overlay-bg-hide');
      this.video_el.play();
    }
    else {


      //Play
      this.overlayElement.classList.remove('video-overlay-bg-hide');
      this.video_el.pause();
    }

    // Change BTN
    btn.classList.toggle('icon-controller-play');
    btn.classList.add('changeButtonIcon');
    btn.classList.toggle('icon-controller-pause');

    setTimeout(function () {

      btn.classList.remove('changeButtonIcon');
    }, 500);
  };

  video.prototype.mute = function (btn) {


    if (btn.classList.contains('icon-volume-high')) {

      //Mute
      this.video_el.muted = true;
    }
    else {

      //Unmute
      this.video_el.muted = false;
    }

    // Change BTN
    btn.classList.toggle('icon-volume-high');
    btn.classList.add('changeButtonIcon');
    btn.classList.toggle('icon-volume-mute2');

    setTimeout(function () {

      btn.classList.remove('changeButtonIcon');
    }, 500);
  };

  video.prototype.updateTimer = function () {

    var all = this.video_el.duration;
    var current = this.video_el.currentTime;

    // Percentage
    var percentage = Math.floor((100 / all) * current);
    this.barLineElement.style.width = percentage + '%';

    // Timer
    if (percentage > 5) {

      this.barCurrentTimeElement.classList.remove('hide');
      this.barCurrentTimeElement.innerHTML = convertToTimeFormat(current);
    }

    if (this.video_el.ended) {

      this.end();
    }
  };

  video.prototype.setPoster = function (){

    var overlay_el;
    var isAppend = false;
    if( document.querySelector('.video-overlay-bg') ){

      overlay_el = document.querySelector('.video-overlay-bg');
    }
    else{

      overlay_el = document.createElement('div');
      isAppend = true;
    }
    overlay_el.className = 'video-overlay-bg';
    overlay_el.style.backgroundImage = 'url(' + this.poster + ')';
    this.overlayElement = overlay_el;

    if( isAppend )
      this.container.appendChild(overlay_el);
  };


  video.prototype.comments = function (data) {

    this.comments.comments_ref = new Videos_Comments(data);
  };

  video.prototype.comments_load = function () {

    // Test Data. It will be replaced by connecting to API.
    var lorem = 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.';
    var data = [
      {name: 'فرید فریدی', text: lorem, date: '۱ فروردین ۱۳۹۵'},
      {name: 'علی دایی', text: lorem, date: '۱ فروردین ۱۳۹۵'}
    ];

    this.comments(data);
  };

  video.prototype.comments_show = function () {

    if( document.body.classList.contains('comments-are-open') ){

      this.comments.comments_ref.close();
      document.body.classList.remove('comments-are-open');
    }
    else{

      this.comments.comments_ref.open();
      document.body.classList.add('comments-are-open');
    }
  };

  video.prototype.createVideo = function () {

    var video_el;
    if (this.container.querySelector('video'))
      video_el = this.container.querySelector('video');
    else
      video_el = document.createElement('video');

    if (this.overlayPoster == false)
      video_el.poster = this.poster;

    video_el.src = this.url;
    var video_source = document.createElement('source');
    video_source.src = this.url;
    video_source.type = 'video/mp4';
    video_el.appendChild(video_source);
    video_el.load();

    if (this.overlayPoster == true) {

      this.setPoster();
    }

    this.video_el = video_el;
    this.container.appendChild(this.video_el);

    // Comments
    this.comments_load();
  };

  video.prototype.createShare = function (fb,twitter) {

    var popup = document.createElement('div');
    popup.className = 'popup-share';
    document.body.appendChild(popup);

    var container = document.createElement('div');
    container.className = 'container';
    popup.appendChild(container);

    var title = document.createElement('span');
    title.textContent = 'Ø¨Ù‡ Ø§Ø´ØªØ±Ø§Ú© Ú¯Ø°Ø§Ø±ÛŒ Ø¯Ø±:';
    container.appendChild(title);

    var facebook = document.createElement('a');
    facebook.href = fb;
    facebook.innerHTML = '<i class="icon-facebook"></i> فیسبوک';
    container.appendChild(facebook);

    var twitter = document.createElement('a');
    twitter.href = fb;
    twitter.innerHTML = '<i class="icon-twitter"></i> توییتر';
    container.appendChild(twitter);

    setTimeout(function () {

      popup.classList.add('show');
    }, 100);

    setTimeout(function () {

      detectOutsideClick('popup-share',function () {

        popup.classList.remove('show');

        setTimeout(function () {

          document.body.removeChild(popup);
        }, 500);
      });

    }, 600);
  };

  video.prototype.createController = function () {

    var controller = document.createElement('div');
    controller.className = 'video-controller';

    // Right
    var right = document.createElement('div');
    right.className = 'right-btns';

    // Volume
    var volume_button = document.createElement('i');
    volume_button.className = 'icon-volume-high';
    right.appendChild(volume_button);

    var right_part = document.createElement('div');
    right_part.className = 'part';
    right.appendChild(right_part);

    //  Comments
    var comments_button = document.createElement('span');
    comments_button.className = 'btn';
    comments_button.innerHTML =  '<i class="icon-message"></i> 2';
    right_part.appendChild(comments_button);

    // Views
    var views_button = document.createElement('span');
    views_button.innerHTML =  '<i class="icon-eye"></i> 100';
    right_part.appendChild(views_button);

    // Share
    var share_button = document.createElement('i');
    share_button.className =  'icon-share';
    right_part.appendChild(share_button);


    // Center
    // Bar
    var bar = document.createElement('div');
    bar.className = 'bar';

    var bar_line = document.createElement('div');
    bar_line.className = 'bar-line';
    bar.appendChild(bar_line);

    var bar_pos = document.createElement('div');
    bar_pos.className = 'bar-pos';
    var bar_currentTime = document.createElement('span');
    bar_currentTime.innerHTML = '00:00';
    bar_currentTime.className = 'hide';
    bar_pos.appendChild(bar_currentTime);
    bar.appendChild(bar_pos);
    this.barLineElement = bar_pos;
    this.barCurrentTimeElement = bar_currentTime;

    var bar_endTime = document.createElement('span');
    bar_endTime.className = 'bar-end-time';
    bar_endTime.innerHTML = '00:00';
    bar.appendChild(bar_endTime);


    // Left
    var left = document.createElement('div');
    left.className = 'left-btns';

    // Play Button
    var play_button = document.createElement('i');
    play_button.className = 'icon-controller-play';
    left.appendChild(play_button);
    this.playButton = play_button;


    // Bundle
    controller.appendChild(right);
    controller.appendChild(bar);
    controller.appendChild(left);

    this.container.appendChild(controller);

    // Events
    var __ = this;

    this.video_el.addEventListener('loadedmetadata', function () {

      bar_endTime.innerHTML = convertToTimeFormat(__.video_el.duration);
    });
    this.video_el.addEventListener('timeupdate', function () {

      __.updateTimer();
    });


    play_button.addEventListener('click', function () {

      __.play();
    });
    volume_button.addEventListener('click', function () {

      __.mute(volume_button);
    });

    comments_button.addEventListener('click', function () {

      __.comments_show();
    });

    share_button.addEventListener('click', function () {

      __.createShare('#','#');// @todo
    });


  };

  video.prototype.changeVideo = function (el){

    this.poster = el.getAttribute('data-poster');
    this.url = el.getAttribute('data-video');
    this.video_el.src = this.url;
    this.video_el.currentTime = 0;
    this.barLineElement.style.width = '0%';
    this.barCurrentTimeElement.classList.add('hide');
    this.barCurrentTimeElement.innerHTML = '00:00';
    this.video_el.load();
    this.setPoster();

    if( this.playButton.classList.contains('icon-controller-pause') || this.playButton.classList.contains('icon-replay') )
      this.playButton.className = 'icon-controller-play';

    // Close Latest
    document.removeEventListener('click', outsideListener);
    document.querySelector('.video-page').classList.remove('latestStatus');
    if( document.querySelector('.current-container') )
      document.querySelector('.current').removeChild(document.querySelector('.current-container'));

    [].slice.call(document.querySelectorAll('.latest-videos li')).forEach(function(vid){

      vid.classList.remove('current');
    });

    el.classList.add('current');

    // autoplay
    this.play();
  };

  video.prototype.latest = function (){

    var videos = document.querySelectorAll('.latest-videos li');
    var that = this;
    [].slice.call(videos).forEach(function(vid){

      vid.addEventListener('click', function(e){

        that.changeVideo(vid);
        e.preventDefault();
      });
    });
  };

  video.prototype.run = function(){

    this.createVideo();
    this.createController();
    this.latest();
  };


  window.video = video;
})(window);

new video();