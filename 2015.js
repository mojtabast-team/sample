/**
 *
 * Sticky
 *
 * I developed this module for setting sticky menu easily in 2015. Old but gold.
 *
 * http://somethingamazing.mojtabast.me/adverbank/index.html
 *
 */

;(function(window){

  var mojtabast_sticky = function(element){

    var options = {};

    // Check parameter
    if( !!element == false || (element.constructor != Array && element.constructor != Object ) ) return;

    if( !!element.length == false ) {
      options = [element];
    }
    else{
      options = element;
    }

    // default values
    var el, op_id=[];
    this.config=[]; // this will keep configure data

    var that = this;

    for(var id= 0; id< options.length; id++){

      op_id = options[id]; // option id

      el = document.querySelector(options[id]['selector']);

      if( !!el == false ) return;

      var values = {
        wrapClass: '', // set wrapper class if you need
        classes: ['fixedHeader'], // Set a/multiple class to element when sticky menu run on it
        fixed: 'onElement', // always: run sticky menu on element when plugin called. onElement: when scroll arrive to element, sticky menu will run
        stopOn: '', // sticky menu will be stop on this element
        direction: 'both' // sticky menu will be stop on this element
      };

      that.config[id] = {};
      that.config[id].el = el; // global

      // initial variables
      that.config[id].data = {};
      that.config[id].options = {};
      that.config[id].options = !!options[id] ? mojtabast.extend( values, options[id]) : values;



      that.setData(id);
      that.init(id);


      (function(i,that) {

        window.onresize = function(){
          that.setData(i);
          that.init(i);
        };

      })(id,that);



    }
  };

  mojtabast_sticky.prototype = {
    setData: function(id){

      // scroll
      this.config[id].data.lastScroll = 0;
      this.config[id].data.directionStatus = false; // when 'fixed_direction' add to element, this var will be true

      // elements
      this.config[id].data.elementW = mojtabast.w(this.config[id].el);
      this.config[id].data.elementH = mojtabast.h(this.config[id].el);
      this.config[id].data.pos = mojtabast.pos(this.config[id].el);
      this.config[id].data.startPos = this.config[id].data.pos.y;
      var stop_el = document.querySelectorAll(this.config[id].options.stopOn)[0];
      this.config[id].data.stopPos = mojtabast.pos(stop_el).y - this.config[id].data.elementH;
    },
    updateData: function(id){
      var stop_el = document.querySelectorAll(this.config[id].options.stopOn)[0];
      this.config[id].data.stopPos = mojtabast.pos(stop_el).y - this.config[id].data.elementH;
    },
    init: function(id){

      var that = this;

      if( this.config[id].options.fixed == 'always'){

        this.show(id);
      }

      if( this.config[id].options.fixed == 'onElement'){


        var pos = this.config[id].data.pos;
        //var pos_x = pos.x;
        var pos_y = pos.y;

        window.addEventListener('scroll', function(){
          //var scroll_x = mojtabast.scroll.x();
          var scroll_y = mojtabast.scroll.y();



          if( !!that.config[id].options.stopOn){

            // Start And End Mode
            that.stopMode(id, scroll_y);
          }
          else{

            // Only Start Mode
            if(scroll_y > pos_y){

              if( that.config[id].data.nav == false ){
                that.show(id);
                that.config[id].data.nav = true;
              }
            }
            else{

              that.hide(id);
              that.config[id].data.nav = false;
            }
          }

        });

      }



    },
    setClass: function(id) {

      // Add classes to element
      for(var i=0; i< this.config[id].options.classes.length; i++){

        this.config[id].el.classList.add(this.config[id].options.classes[i]);
      }
    },
    resetClass: function(id) {

      // Remove classes from element
      for(var i=0; i< this.config[id].options.classes.length; i++){

        this.config[id].el.classList.remove(this.config[id].options.classes[i]);
      }
    },
    wrap: function(id){

      // Wrap
      var div = document.createElement('div');
      if( !!this.config[id].options.wrapClass ){

        div.classList.add(this.config[id].options.wrapClass);
      }
      div.classList.add('wrapper_wrapper_wrapper'); // this will use for identify

      // Set width and height
      div.style.width = this.config[id].data.elementW;
      div.style.height = this.config[id].data.elementH;

      mojtabast.wrap(div,this.config[id].el);
    },
    unwrap: function(id){

      // Wrapper
      var wrapper =  this.config[id].el.parentNode;
      if( wrapper.classList.contains('wrapper_wrapper_wrapper') ){

        mojtabast.unwrap(this.config[id].el);
      }

    },
    scrollDirection: function(id,scrollY){
      var output;

      if( scrollY < this.config[id].data.lastScroll ){
        output = 'up';
      }else{
        output =  'down';
      }

      this.config[id].data.lastScroll = scrollY;

      return output;
    },
    stopMode: function(id, scrollY){
      var that = this;
      var start = this.config[id].data.startPos;
      var stop = this.config[id].data.stopPos;
      this.updateData(id);

      if( start < scrollY){

        if(  stop  >  scrollY ){
          // On

          if( !!this.config[id].data.nav === false ){

            // Direction
            if( this.config[id].options.direction == 'both' || this.scrollDirection(id,scrollY) == this.config[id].options.direction ){
              this.config[id].data.nav = true;
              this.show(id);

              if( !this.config[id].data.directionStatus && this.config[id].options.direction != 'both' ) {
                this.config[id].el.classList.add('fixed_direction');

                setTimeout(function () {
                  that.config[id].el.classList.remove('fixed_direction');
                }, 30);

                this.config[id].data.directionStatus = true;
              }


            }

          }
          else{

            if( this.config[id].options.direction != 'both' && this.scrollDirection(id,scrollY) != this.config[id].options.direction ){
              this.config[id].data.nav = false;
              this.config[id].data.directionStatus = false;
              this.config[id].el.classList.add('fixed_direction');
            }

          }
        }
        else{
          // bottom
          if( !!this.config[id].data.nav === true ) {


            this.config[id].data.nav = false;
            this.pause(id);
          }
        }

      }
      else{

        // top
        if( !!this.config[id].data.nav){
          this.config[id].data.nav = false;
          this.hide(id);
        }

      }

      return true;
    },
    show: function(id){

      this.config[id].el.setAttribute('style','');
      this.unwrap(id);
      this.setClass(id);
      this.wrap(id);
    },
    hide: function(id){
      this.config[id].el.setAttribute('style','');
      this.resetClass(id);
      this.unwrap(id);
    },
    pause: function(id){
      var stop_el = document.querySelectorAll(this.config[id].options.stopOn)[0];

      if( !!stop_el ){
        var stopPos = this.config[id].data.stopPos;
        var H = this.config[id].data.elementH;
        this.config[id].el.style.top = stopPos;
        this.config[id].el.style.position = 'absolute';
      }

    }
  };


  window.mojtabast_sticky = mojtabast_sticky;
})(window);

/*
 EXAMPLE

 var options =[
 {
 'selector' : '.tab-a',
 'stopOn': '.flow',
 'classes': ['tabs_fixed']
 },
 {
 'selector' : '.tab-b',
 'stopOn': '.blog-posts',
 'classes': ['tabs_fixed']
 }
 ];
 var s = new mojtabast_sticky(options);
 var s2 = new mojtabast_sticky('.AA', {'stop': '.blog-posts','classes': ['tabs_fixed']});
 */