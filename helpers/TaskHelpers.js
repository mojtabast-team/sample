import React from 'react';
import PropTypes from 'prop-types'
import {connect} from "react-redux";

import {save_task_transaction, sync_task_transaction} from '../../actions/local'

import Confirmation from '../../containers/Confirmation'

import {transaction_creator} from '../../utils/tasks'

import {CONFIRMATIONS_TYPES} from '../../constants/config'
import {PICTURE_UPLOAD, PICTURE_DOWNLOAD} from '../../constants/api'


function helper( Component ){


  class TaskHelpers extends React.Component {

    state = {

      departmentID: this.context.router.route.match.params.departmentID,
      checklistID: this.context.router.route.match.params.checklistID,
      taskID: this.context.router.route.match.params.taskID,
      task: {},

      ActionBar_Buttons: [],
    };

    static contextTypes = {

      router: PropTypes.object,
      setButtons: PropTypes.func,
      FullPagePopup: PropTypes.func,
      fileStorage: PropTypes.object,
    };


    render() {

      const props = {

        ...this.props,

        setTask: this.setTask,
        task: this.state.task,
        initTask: this.initTask,
        declineTask: this.handleDecline,
        finishTask: this.handleFinish,
      };

      return <Component {...props} />;
    }


    /**
     *
     * Task helper needs to set a task as base task, task will set by using react-router but when we need some of the methods ( sharing ), We should set task manually.
     *
     * @param taskID
     * @returns {Promise}
     */
    setTask = (taskID) => {

      let id = taskID ? taskID : this.context.router.route.match.params.taskID;

      return new Promise( (resolve, reject) => {

        this.setState({

          taskID: id
        }, () => {

          this.getTaskInfo().then(resolve);
        });

      } );
    };


    /**
     *
     * Initialing task for first time
     *
     */
    initTask = () =>  {

      this.getTaskInfo().then( () => {

        this.initActionBar()
      });

    };


    /**
     *
     * If user had cached task data (changes), We should merge the cashed data with original data
     *
     * @param task
     * @returns {*}
     */
    mergeTaskTransactions = (task) => {

      // Replace saved task transaction with original data.
      const task_transaction = this.props.tasks_transactions[task._id];

      if( task_transaction ) {

        task.resultValuesGiven = task_transaction.resultValuesGiven;
        task.requiredSignaturesGiven = task_transaction.requiredSignaturesGiven;
      }

      return task;
    };

    /**
     *
     * Find and set task
     * @returns {Promise}
     */
    getTaskInfo = () => {

      // const taskID = optionalTaskID ? optionalTaskID ? this.state.taskID;

      return new Promise( (resolve, reject) => {


        const task = Array.isArray(this.props.tasks) ? this.props.tasks.find( (task) => {

          return task._id == this.state.taskID
        } ) : false;


        this.setState({

          task: this.mergeTaskTransactions(task)
        }, resolve);

      } );

    };


    /**
     *
     * When confirmation is available for a task, this method will open the confirmations
     *
     */
    openConfirmations = () => {

      // Merge Signature and Result Values.
      let { resultValuesGiven, requiredSignaturesGiven, requiredSignatures} = this.state.task;

      const signatures = requiredSignaturesGiven == requiredSignatures ? requiredSignaturesGiven : requiredSignatures;

      requiredSignaturesGiven = signatures.map( (signature) => {

        return {

          type: 'SIGNATURE',
          ...signature,
        };
      } );

      // Prepare Data
      const confirmationData = [
        ...resultValuesGiven,
        ...requiredSignaturesGiven,
      ];


      // console.log('confirmation original data', confirmationData, this.props.tasks_transactions);

      return new Promise( (resolve, reject) => {

        const finish = () => {

          this.finish().then( resolve );
        };


        this.context.FullPagePopup(
          <Confirmation

            type="task"
            data={confirmationData}
            changeActionBar={this.changeActionBar}
            onCloseConfirmation={this.handleCloseConfirmation}
            onFinish={finish}
            onChange={this.handleConfirmationChange}
          />
        );

      } );
    };

    /**
     *
     * When a task is done, this will be triggered
     */
    handleDone = () => {

      const checklistID = this.state.checklistID;
      const taskID = this.state.task._id;
      const data = {

        finished: true,
        success: true,
      };

      const transactions = transaction_creator(checklistID, taskID, data);

      // console.log('Transaction: ',transactions);
      this.props.save_task_transaction(taskID, transactions);

    };


    /**
     * Syncing task data
     * @returns {Promise}
     */
    startSyncing = () => {


      return new Promise((resolve, reject) => {


        let {_id} = this.state.task;
        const task_transaction = this.props.tasks_transactions[_id];

        // Export pictures from resultValues.
        let resultValuesGiven = task_transaction && task_transaction.resultValuesGiven ? [...task_transaction.resultValuesGiven] : [];
        let Picture_queue = [];


        resultValuesGiven.forEach( (item, i) => {

          if( item.type == CONFIRMATIONS_TYPES.PICTURE ){

            if( item.localPath && item.isUploaded == false ) {

              Picture_queue.push({

                index: i, // We don't need ID for ResultValues, So we should consider the array index for an ID-like behaviour.
                localPath: item.localPath
              });
            }
          }

        } );



        // Check a resultValues has Picture or not.
        if( Picture_queue.length > 0 ){

          let pictures_uploaded_data = [];

          // Start uploading and storing them using Promises.
          const Picture_promises = Picture_queue.map( (item) => {

            return this.context.fileStorage.upload({

              uri: item.localPath,
              endpoint: PICTURE_UPLOAD(this.state.departmentID),
              downloadLink: PICTURE_DOWNLOAD.bind(null, this.state.departmentID)
            }).then( (data) => {

              pictures_uploaded_data.push({

                index: item.index,
                file: data
              });

            } );

          });


          // When all the file finished, We need to start syncing task transactions now.
          Promise.all(Picture_promises).then( () => {

            // First we replace the uploaded data with local data in the resultValues.
            pictures_uploaded_data.forEach( (item) => {

              resultValuesGiven[item.index].pictureName = item.file.uri.split('/').pop();
              resultValuesGiven[item.index].localPath = item.file.path;
              resultValuesGiven[item.index].isUploaded = true;

            } );

            // Let's start syncing.
            this.props.save_task_transaction(_id, {

              resultValuesGiven
            });

            this.props.sync_task_transaction(this.state.departmentID);
            resolve();
          });

        }
        else{

          // console.log('Don\'t need to upload.');
          this.props.sync_task_transaction(this.state.departmentID);
          resolve();
        }

      })
    };


    /**
     * Finish a task
     * @returns {Promise|*|Promise.<U>|Promise.<TResult>}
     */
    finish = () => {

      //
      this.handleDone();

      // Close Popup
      this.handleCloseConfirmation();


      this.setState({

        task: {

          ...this.state.task,
          finished: true,
          success: true,
        }
      }, this.initActionBar);


      return this.startSyncing().then(() => {

        // If task has a picture resultValue, usually it takes time to upload picture. For this reason when user finish a task, Action Bar will not update and user think he should do it again.

        // this.setState({
        //
        //   task: {
        //
        //     ...this.state.task,
        //     finished: true,
        //     success: true,
        //   }
        // }, this.initActionBar);

      });
    };

    /**
     * When user is filled all of the confirmations
     */
    handleFinish = () => {

      const resultValues = this.state.task.resultValuesGiven;
      const requiredSignatures = this.state.task.requiredSignatures;
      const shouldShowConfirmation = resultValues.length + requiredSignatures.length;

      // const howMuchIsDone = this.state.task.reduce( (sum, value) => {
      //
      //
      // }, 0 );

      if( shouldShowConfirmation ){

        return this.openConfirmations()
      }
      else{


        return this.finish();
      }

    };


    /**
     * We merge signature and resultValues in the Confirmation,
     * So We need to convert back to a API-wanted style.
     *
     * @param confirmations_data
     * @returns {{resultValuesGiven: Array, requiredSignaturesGiven: Array}}
     */
    confirmationsToTransaction = ( confirmations_data ) => {

      let resultValuesGiven = [];
      let requiredSignaturesGiven = [];


      confirmations_data.forEach( (item) => {

        if( item.type == CONFIRMATIONS_TYPES.SIGNATURE ){

          requiredSignaturesGiven.push(item);
        }

        else {

          resultValuesGiven.push(item);
        }
      } );


      return {

        resultValuesGiven,
        requiredSignaturesGiven,
      };

    };


    /**
     *
     * When user click on 'Close' button in the action bar
     */
    handleCloseConfirmation = () => {

      // Close Popup
      this.context.FullPagePopup();
      this.initActionBar();
    };

    /**
     *
     * When data will change in the confirmation, this method will triggered
     */
    handleConfirmationChange = (data) => {

      const checklistID = this.state.checklistID;
      const taskID = this.state.task._id;

      data = this.confirmationsToTransaction(data);
      const transactions = transaction_creator(checklistID, taskID, data);

      // console.log('data', transactions);
      this.props.save_task_transaction(taskID, transactions);
    };


    /**
     *
     * When a task is finshed, user can press on reopen in the action bar and this method will triggered.
     */
    handleReopen = () => {

      const checklistID = this.state.checklistID;
      const taskID = this.state.task._id;
      const data = {

        finished: false,
        success: false,
      };

      const transactions = transaction_creator(checklistID, taskID, data);

      this.props.save_task_transaction(taskID, transactions);


      this.startSyncing().then(() => {

        this.setState({

          task: {

            ...this.state.task,
            ...data
          }
        }, this.initActionBar);

      });


    };


    /**
     *
     * When data will change in the confirmation, this method will triggered
     */
    handleDeclineChange = (data) => {

      const checklistID = this.state.checklistID;
      const taskID = this.state.task._id;

      const transactions = transaction_creator(checklistID, taskID, {

        notes: data[0].stringValue,
        finished: true,
        success: false,
      });

      // console.log('data', transactions);
      this.props.save_task_transaction(taskID, transactions);
    };

    /**
     *
     * When user press finish button in confirmations for task declining.
     * @returns {Promise.<U>|Promise.<TResult>}
     */
    handleDeclineFinish = () => {

      // Close Popup
      this.handleCloseConfirmation();

      return this.startSyncing().then(() => {

        // this.initActionBar();

        this.setState({

          task: {

            ...this.state.task,
            finished: true,
            success: false,
          }
        }, this.initActionBar);

      });

    };

    /**
     *
     * When user press decline in action bar in task.
     * @returns {Promise}
     */
    handleDecline = () => {

      let {_id} = this.state.task;
      const task_transaction = this.props.tasks_transactions[_id];

      const confirmationData = [
        {

          type: 'STRING',
          description: 'Why?',
          stringValue: task_transaction && task_transaction.notes ? task_transaction.notes : '',
        }
      ];


      return new Promise( (resolve, reject) => {

        const finish = () => {

          this.handleDeclineFinish().then( resolve );
        };

        this.context.FullPagePopup(
          <Confirmation

            type="task"
            data={confirmationData}
            changeActionBar={this.changeActionBar}
            onCloseConfirmation={this.handleCloseConfirmation}
            onFinish={finish}
            onChange={this.handleDeclineChange}
          />
        );
      } );

    };


    initActionBar = () => {

      // Init Buttons
      let normal_btns = [
        {
          text: 'Decline',
          icon: 'close',
          action: this.handleDecline
        },
        {
          text: 'Done',
          icon: 'check',
          action: this.handleFinish
        },
      ];

      let finished_btns = [
        {
          text: 'Reopen',
          icon: 'check',
          action: this.handleReopen
        }
      ];


      // console.log('Action Bar', this.state, this.state.task, this.state.task.finished);
      // Set state
      this.setState({

        ActionBar_Buttons: this.state.task.finished ? finished_btns : normal_btns
      }, this.updateActionBar);

    };

    updateActionBar = () => {

      this.context.setButtons(this.state.ActionBar_Buttons);
    };

    changeActionBar = (buttons) => {

      this.setState({

        ActionBar_Buttons: buttons
      }, this.updateActionBar);
    };

  }


  const mapStateToProps = (state) => {

    return {

      connection_status: state.temp.connection_status,
      tasks: state.db.tasks,
      tasks_transactions: state.local.tasks
    };
  };

  const mapDispatchToProps = (dispatch) => {

    return {

      sync_task_transaction: (departmentID) => {

        dispatch(sync_task_transaction(departmentID));
      },

      save_task_transaction: (taskID, data) => {

        dispatch(save_task_transaction(taskID, data));
      }
    };
  };



  return connect(mapStateToProps, mapDispatchToProps)(TaskHelpers);
}

export default helper;